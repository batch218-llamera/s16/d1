// console.log("Hello world");

// [SECTION] Assignment Operator
	// Basic Assignment Operator (=)
	// The assignment operator assigns the value of the right operand to a variable
	let assignmentNumber = 8;
	console.log("The value of the assignmentNumber variable: " + assignmentNumber);

// [SECTION] Arithmetic Operations

	let x = 200;
	let y = 18;

	console.log("x: " + x);
	console.log("y: " + y);
	console.log("--------")
	
	// Adition
	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	// Subtraction
	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	// Multiplication
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	// Division
	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	// Remainder
	let modulo = x % y;
	console.log("Result of modulo operator: " + modulo);

// Continuation of assignment operator

// Addition Assignment Operator
	// Syntax for long method

	// current value of assignmentNumber is 8
	// assignmentNumber = assignmentNumber + 2;
	// console.log(assignmentNumber);

	// Syntax for short method
	assignmentNumber +=2;
	console.log("Result of Addition Assignment Operator: " + assignmentNumber); // assignmentNumber value: 10

	// assignmentNumber = assignmentNumber - 3;
	// console.log(assignmentNumber); //7

// Subtraction Assignment Operator
	assignmentNumber -=3;
	console.log("Result of Subtraction Assignment Operator: " + assignmentNumber);

// Multiplication Assignment Operator
	assignmentNumber *= 2;
	console.log("Result of Multiplication Assignment Operator: " + assignmentNumber);

// Division Assignment Operator
	assignmentNumber /= 2;
	console.log("Result of Division Assignment Operator: " + assignmentNumber);


// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);
	/*
		The operations were done in the following order:
		1. 3 * 4 = 12 | 1 + 2 - 12 / 5
		2. 12 / 5 = 2.4 | 1 + 2 - 2.4
		3. 1 + 2 = 3 | 3 - 2.4
		4. 3 - 2.4 = 0.60

	*/

let pemdas = (1+(2-3)) * (4/5);
console.log("Result of pemdas operation: " + pemdas);

// Hierarchy
// Combinations of multiple arithmetic operators will follow the pemdas rule
/*
	1. Parenthesis
	2. Exponent
	3. Multiplication or division
	4. Addition or subtraction

	NOTE: Will also follow left to right rule
*/

// [SECTION] Increment and Decrement
// Increment - increasing
// Decrement - decreasing

// Operators that add or subtract values by 1 and reassign the value of the variable where the increment (++) or decrement (--) was applied.

let z = 1;

// pre-increment - it will take effect immediately
let increment = ++z;
console.log("Result of pre-increment: " + increment); // 2
console.log("Result of pre-increment of z: " + z); // 2

// post-increment - it only will take effect after 1st execution
increment = z++;
console.log("Result of post-increment: " + increment); // 2
console.log("Result of post-increment of z: " + z); // 3

/*increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment of z: " + z);
*/

// pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement); // 2
console.log("Result of pre-decrement of z: " + z); // 3

// post-decrement
decrement = z--;
console.log("Result of pre-decrement: " + decrement); // 2
console.log("Result of pre-decrement of z: " + z); // 1

// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another

	// combination of number and string data type will result to string
	let numA = 10; // number
	let numB = "12"; // string

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	// combination of number and number data type will result to number
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	// combination of number and boolean data type will result to number
	let numX = 10;
	let numY = true; 
			// boolean values are
				// true = 1;
				// false = 0;
	coercion = numX + numY;
	console.log(coercion);
	console.log(typeof coercion);

	let num1 = 1;
	let num2 = false;
	coercion = num1 + num2;
	console.log(coercion);
	console.log(typeof coercion);

// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands
// After evaluation, it returns a boolean value

// equality operator (== - also read as equal to)
// compares the value, but not the data type
console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == "1"); // true
console.log(0 == false); // true // false boolean value is also equal to zero

let juan = "juan";
console.log('juan' == 'juan'); // true
console.log('juan' == 'Juan'); // false // equality operator is strict with letter casing
console.log(juan == 'juan'); // true

// inequality operator (!= - also read as not equal) 
console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != "1"); // false
console.log(0 != false); // false
console.log('juan' != 'juan'); // false
console.log('juan' != 'Juan'); // true
console.log(juan != 'juan'); // false

// [SECTION] Relational Operators
// Some comparison operators to check whether one value is greater or less than the other value
// It will return the value of true or false

console.log("-----------")

let a = 50;
let b = 65;

// GT or Greater Than Operator (>)
let isGreaterThan = a > b; // 50 > 65 = false
console.log(isGreaterThan);

// LT or Less Than Operator (<)
let isLessThan = a < b; // 50 < 65 = true
console.log(isLessThan);

// GTE or Greater Than or Equal Operator (<=) - need to satisfy one condition
let isGTorEqual = a >= b; // false 
console.log(isGTorEqual);

// LTE or Less Than or Equal Operator (<=)
let isLTorEqual = a < b; // true
console.log(isLTorEqual);

// Forced Coercion
let num = 56;
let numStr = "30";
console.log(num > numStr); // true
// Forced coercion to change string to number

let str = "twenty";
console.log(num >= str); // false
// Since the string is not numeric, the string will not be converted to a number, or a.k.a NaN (Not a Number)

// Logical AND Operator - must satisfy all condition to be true
let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered; // false
console.log("Result of logical AND operator: " + allRequirementsMet); 
// AND operator requires all / both are true

// Logical OR Operator - satisfy only one condition to be true
let someRequirementsMet = isLegalAge || isRegistered; // true
console.log("Result of logical OR operator: " + someRequirementsMet);
// OR operator requires only 1 true

// Logical NOT Operator (!)
// Returns the opposite value

let someRequirementsNotMet = !isLegalAge; // false
console.log("Result of logical NOT operator: " + someRequirementsNotMet);






















